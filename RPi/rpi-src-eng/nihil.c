#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_keysym.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_image.h>

#define KB_SCROLL_STEP 16	//step for up/down key scrolling
#define MS_SCROLL_STEP 10	//step for mouse wheel scrolling
#define MOUSE_SUPPORT 1		//compile mouse wheel scrolling support
#define _min(x,y) ((x)<=(y)?(x):(y))
#define _max(x,y) ((x)<=(y)?(y):(x))
/* SDL interprets each pixel as a 32-bit number, so our masks must depend
on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
Uint32 rmask = 0xff000000;
Uint32 gmask = 0x00ff0000;
Uint32 bmask = 0x0000ff00;
Uint32 amask = 0x000000ff;
#else
Uint32 rmask = 0x000000ff;
Uint32 gmask = 0x0000ff00;
Uint32 bmask = 0x00ff0000;
Uint32 amask = 0xff000000;
#endif

int POS = 61;
SDL_Surface *screen = NULL;		//on-screen surface
SDL_Surface *optimg = NULL;		//loaded image optimized for screen bpp
SDL_Event event;				//SDL events for keyboard and mouse control
TTF_Font *font = NULL;			//font for some other text
TTF_Font *sfont = NULL;			//font for scroller
static SDL_Color text_color_white = { 255, 255, 255 };
static SDL_Color text_color_yellow = { 128, 127, 122 };
int link_visible = 0;			//link is visible on the screen(enter can react?)
int loc = 1;					//location nr
int next_loc[4];				//choice of next locations,0 ended
int pnl;						//pointer to next location array[tab changed]
volatile int linkon = 0;
volatile int linkdraw = 0;
volatile int scrolldraw = 0;
double phi=0;
char quit_t[]=" EXIT? [Y]es/[N]o ";
char l1_t[]="l1.png",l2_t[]="l2.png",l3_t[]="l3.png",l4_t[]="l4.png",l5_t[]="l5.png",l6_t[]="l6.png";
char l7_t[]="l7.png",l8_t[]="l8.png",l9_t[]="l9.png",l10_t[]="l10.png",l11_t[]="la.png",l12_t[]="lb.png";
char l13_t[]="lc.png",l14_t[]="ld.png",l15_t[]="le.png",l16_t[]="lf.png",l17_t[]="lg.png",l18_t[]="lh.png";
char l19_t[]="li.png",l20_t[]="lj.png",l21_t[]="lk.png",l22_t[]="ll.png";
char scrollt[]="                           ""In nihilum reverteris"" - (C) 2018 - another text-game / interactive novel by Yerzmyey. Text and music by Yerzmyey/H-PRG. Graphic by Habib^Joulo/H-PRG. Code by Hellboj/H-PRG. Help - Gasman/H-PRG. Controls: TAB - hyperlink selection. ENTER - fire / approval of the choice. Cursors UP/DOWN - scrolling the text. Betatesters: Kya, Vocoderion, Radxcell/Illusion, Hellboj. Scientific consultation (physics and mathematics) - Dr. Hellboj. Moral support - Piotr Marecki. The English version has been based on the initial translation made by Caryl Swift and Peter Foulds of Lingua Lab www.lingualab.pl which has been fully sponsored by Yerzmyey. This publication has been financed within a program of the Polish Minister of Science and Higher Education under the name ""National Program for the Development of Humanities"" in 2016-2019 (number 0020/NPRH4/H2b/83/2016).                                                                                  ";


//rectangles for blitting location onto the screen
SDL_Rect srcrect;
SDL_Rect dstrect;
//rectangle for selected link to render effect
SDL_Rect linkrect;
//location's links' rectangles
SDL_Rect links[3];

void render_text(SDL_Surface *dst,TTF_Font *font, char *text, Sint16 x, Sint16 y)
{
	SDL_Surface *tmp;
	SDL_Rect dstrect;

	tmp = TTF_RenderText_Blended(font, text, text_color_white);
	if (tmp != NULL) {
		dstrect.x = x;
		dstrect.y = y;
		dstrect.w = tmp->w;
		dstrect.h = tmp->h;
		linkon = 0;
		while(linkdraw) ;
		SDL_BlitSurface(tmp, NULL, dst, &dstrect);
		SDL_FreeSurface(tmp);
		SDL_Flip(dst);
	}
}

void SDLDrawPixel(SDL_Surface *screen, Sint32 x, Sint32 y, char r, char g, char b)
{
	Uint32 color = SDL_MapRGB(screen->format, r, g, b);

	if (SDL_MUSTLOCK(screen)) {
		if (SDL_LockSurface(screen) < 0) {
			return;
		}
	}

	switch (screen->format->BytesPerPixel) {
	case 1: { /* Assuming 8-bpp */
		Uint8 *bufp;
		bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
		*bufp = color;
	}
	break;

	case 2: { /* Probably 15-bpp or 16-bpp */
		Uint16 *bufp;

		bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
		*bufp = color;
	}
	break;

	case 3: { /* Slow 24-bpp mode, usually not used */
		Uint8 *bufp;

		bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
		*(bufp+screen->format->Rshift/8) = r;
		*(bufp+screen->format->Gshift/8) = g;
		*(bufp+screen->format->Bshift/8) = b;
	}
	break;

	case 4: { /* Probably 32-bpp */
		Uint32 *bufp;

		bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
		*bufp = color;
	}
	break;
	}

	if (SDL_MUSTLOCK(screen)) {
		SDL_UnlockSurface(screen);
	}
	SDL_UpdateRect(screen, x, y, 1, 1);
}

//load location bmp into the optimg surface
int load_location(char *bfname)
{
	SDL_Surface *image = IMG_Load(bfname);
	if (image == NULL)
	{
		printf("Cannot load image %s! SDL Error: %s\n",bfname, SDL_GetError());
		return -1;
	}

	optimg = SDL_ConvertSurface(image, screen->format,0);

	if( optimg == NULL )
	{
		printf("Unable to optimize image %s! SDL Error: %s\n", bfname, SDL_GetError());
	}

	SDL_FreeSurface(image);
}
Uint32 scroll_text(Uint32 interval, void* bgimg)
{
	static int position=0;
	static int tpos=0;
	if (scrolldraw) return interval;
	scrolldraw = 1;
	SDL_Surface *buff;
	SDL_Surface *buffo;
	SDL_Rect imgrect,bufrect,scrrect;
	imgrect.x = 100-position;
	imgrect.y = 955;
	imgrect.w = 1920;
	imgrect.h = 124;//116
	bufrect.x = 0;
	bufrect.y = 0;
	bufrect.w = 2120;
	bufrect.h = 124;//116
	srcrect.x = 100-position;
	srcrect.y = 955;
	buff = SDL_CreateRGBSurface(SDL_SWSURFACE, 2120, 124, 32, rmask, gmask, bmask, amask);
	buffo = SDL_ConvertSurface(buff, screen->format,0);
	SDL_BlitSurface((SDL_Surface*)bgimg,&imgrect,buffo,NULL);	
		SDL_Surface *tmp;
		SDL_Rect dstrect;
		char text[34];
		snprintf(text,32,"%s",scrollt+tpos);
//		printf("%s\n",text);
		tmp = TTF_RenderText_Blended(sfont, text, text_color_yellow);
		if (tmp != NULL) {
			dstrect.x = 0;
			dstrect.y = 0;
			dstrect.w = tmp->w;
			dstrect.h = tmp->h;
			SDL_BlitSurface(tmp, NULL, buffo, &dstrect);
			SDL_FreeSurface(tmp);
		}
	position+=8;
	if (position >= POS)
	{
		position = 0;
		tpos++;
		if (tpos >= 760) tpos = 0;
	}
	SDL_BlitSurface(buffo,NULL,screen,&srcrect);
	SDL_Flip(screen);
	SDL_FreeSurface(buffo);
	SDL_FreeSurface(buff);
	scrolldraw = 0;
	return interval;
}

void intro(void)
{
	SDL_Surface *image = IMG_Load("0START.png");
	if (image == NULL)
	{
		printf("Cannot load image %s! SDL Error: %s\n","0START.bmp", SDL_GetError());
	}

	optimg = SDL_ConvertSurface(image, screen->format,0);

	if( optimg == NULL )
	{
		printf("Unable to optimize image %s! SDL Error: %s\n", "0START.bmp", SDL_GetError());
	}

	SDL_Rect srcrecti;
	SDL_Rect dstrecti;
	SDL_Rect srect;

	srcrecti.x = 0;
	srcrecti.y = 0;
	srcrecti.w = 1920;
	srcrecti.h = 1080;
	dstrecti.x = 0;
	dstrecti.y = 0;
	dstrecti.w = 1920;
	dstrecti.h = 1080;
	
	srect.x = 100;
	srect.y = 0;
	srect.w = 1720;
	srect.h	= 1080;

	SDL_FreeSurface(image);
	SDL_BlitSurface(optimg,&srcrecti,screen,&dstrecti);
    SDL_Flip(screen);
	SDL_TimerID stimerID = SDL_AddTimer(25, scroll_text, optimg);
	int dec=0;
	SDL_SetClipRect(screen, &srect);
	while(!dec)
	{
		while (SDL_PollEvent(&event))
			if (event.type == SDL_KEYDOWN) 
			{
//				if (event.key.keysym.sym == SDLK_LEFT) POS--;
//				if (event.key.keysym.sym == SDLK_RIGHT) POS++;
//				if (event.key.keysym.sym == SDLK_ESCAPE) dec++;
//				printf("%d\n",POS);
				dec++;
			}
	}
	SDL_RemoveTimer(stimerID);
	while(scrolldraw) ;
	SDL_FreeSurface(optimg);
	SDL_SetClipRect(screen, NULL);
}

Uint32 render_link(Uint32 interval, void* scrn)
{
	if (linkdraw) return interval;
	if (!linkon) return interval;
	linkdraw = 1;
	phi+=0.01;
	SDL_Surface *screen;
	SDL_Surface *buff;
	SDL_Surface *buffo;
	SDL_Rect imgrect;
	link_visible = 0;
	int x,y,r,g,b,i,j,maxdot;
	int sx,sy,sw,sh;	//screen viewport for effect
	screen = (SDL_Surface*) scrn;

	for (j = 0; j<4; j++)
	{
		if (next_loc[j] == 0) break;
		if (srcrect.y+1080 <= links[j].y) break;
		sx = links[j].x;
		sy = links[j].y;
		sw = links[j].w;
		sh = _min(links[j].h,srcrect.y+1080-links[j].y);
		imgrect.x=sx;
		imgrect.y=sy;
		imgrect.w=sw;
		imgrect.h=sh;
	    buff = SDL_CreateRGBSurface(SDL_SWSURFACE, sw, sh, 32, rmask, gmask, bmask, amask);
		buffo = SDL_ConvertSurface(buff, screen->format,0);
		SDL_BlitSurface(optimg,&imgrect,buffo,NULL);
//Lissajouse params for selected & unselected links
#define _as 3.
#define _bs 10.
#define _an 12.
#define _bn 20.
#define _PI 3.1415926535897932384626433832795
#define _RES (4*1024)

		double A,B,t;
		
		A = (double)sw/2.; B = (double)sh/2.;
		for (i = 0; i<= _RES; i++)
		{
			t = (double)i/_RES*2.*_PI;
			r = g = b = 0;
			if (j == pnl)
			{
				x = A+(A-1)*sin(_as*t+phi);
				y = B+(B-1)*sin(_bs*t);
				r = 255;//(double)rand()*255.0/(double)RAND_MAX;
				link_visible = 1;
			}
			else 
			{
				x = A+(A-1)*sin(_an*t+phi);
				y = B+(B-1)*sin(_bn*t);
				b = 255;//(double)rand()*255.0/(double)RAND_MAX;
			}
			SDLDrawPixel(buffo,x,y,r,g,b);
		}
		imgrect.y=sy-srcrect.y;
		SDL_BlitSurface(buffo,NULL,screen,&imgrect);
		SDL_Flip(screen);
		SDL_FreeSurface(buffo);
		SDL_FreeSurface(buff);
	}
	linkdraw = 0;
	return interval;
}

void set_loc(int location)
{
	linkon = 0;
	while(linkdraw) ;
	next_loc[0]=0;
	next_loc[1]=0;
	next_loc[2]=0;
	next_loc[3]=0;
	switch (location)
	{
		case 1:
			load_location(l1_t);
			next_loc[0]=2;
			links[0].x=386;
			links[0].y=2179;
			links[0].w=787-386;
			links[0].h=2209-2179;
		break;
		case 2:
			load_location(l2_t);
			next_loc[0]=3;
			next_loc[1]=4;
			links[0].x=480;
			links[0].y=240;
			links[0].w=900-480;
			links[0].h=265-240;
			links[1].x=167;
			links[1].y=278;
			links[1].w=578-167;
			links[1].h=304-278;
		break;
		case 3:
			load_location(l3_t);
			next_loc[0]=5;
			links[0].x=1275;
			links[0].y=742;
			links[0].w=1800-1275;
			links[0].h=770-742;
		break;
		case 4:
			load_location(l4_t);
			next_loc[0]=5;
			links[0].x=1190;
			links[0].y=894;
			links[0].w=1550-1190;
			links[0].h=926-894;
		break;
		case 5:
			load_location(l5_t);
			next_loc[0]=6;
			next_loc[1]=7;
			links[0].x=557;
			links[0].y=2370;
			links[0].w=800-557;
			links[0].h=2402-2370;
			links[1].x=126;
			links[1].y=2410;
			links[1].w=450-126;
			links[1].h=2434-2410;
		break;
		case 6:
			load_location(l6_t);
			next_loc[0]=8;
			links[0].x=725;
			links[0].y=2437;
			links[0].w=935-725;
			links[0].h=2464-2437;
		break;
		case 7:
			load_location(l7_t);
			next_loc[0]=8;
			links[0].x=674;
			links[0].y=1782;
			links[0].w=896-674;
			links[0].h=1807-1782;
		break;
		case 8:
			load_location(l8_t);
			next_loc[0]=9;
			next_loc[1]=10;
			links[0].x=1352;
			links[0].y=3289;
			links[0].w=1786-1352;
			links[0].h=3322-3289;
			links[1].x=782;
			links[1].y=3328;
			links[1].w=1042-782;
			links[1].h=3354-3328;
		break;
		case 9:
			load_location(l9_t);
			next_loc[0]=11;
			next_loc[1]=12;
			next_loc[2]=13;
			links[0].x=175;
			links[0].y=1164;
			links[0].w=517-175;
			links[0].h=1196-1164;
			links[1].x=43;
			links[1].y=1243;
			links[1].w=517-43;
			links[1].h=1274-1243;
			links[2].x=43;
			links[2].y=1435;
			links[2].w=439-43;
			links[2].h=1466-1435;
		break;
		case 10:
			load_location(l10_t);
			next_loc[0]=1;
			links[0].x=1324;
			links[0].y=780;
			links[0].w=1712-1324;
			links[0].h=810-780;
		break;
		case 11:
			load_location(l11_t);
			next_loc[0]=14;
			links[0].x=552;
			links[0].y=1704;
			links[0].w=1043-552;
			links[0].h=1736-1704;
		break;
		case 12:
			load_location(l12_t);
			next_loc[0]=14;
			links[0].x=1164;
			links[0].y=2053;
			links[0].w=1568-1164;
			links[0].h=2084-2053;
		break;
		case 13:
			load_location(l13_t);
			next_loc[0]=14;
			links[0].x=1050;
			links[0].y=2052;
			links[0].w=1236-1050;
			links[0].h=2078-2052;
		break;
		case 14:
			load_location(l14_t);
			next_loc[0]=15;
			links[0].x=1332;
			links[0].y=1638;
			links[0].w=1836-1332;
			links[0].h=1670-1638;
		break;
		case 15:
			load_location(l15_t);
			next_loc[0]=16;
			next_loc[1]=17;
			links[0].x=1408;
			links[0].y=3535;
			links[0].w=1874-1408;
			links[0].h=3565-3535;
			links[1].x=449;
			links[1].y=3612;
			links[1].w=854-449;
			links[1].h=3642-3612;
		break;
		case 16:
			load_location(l16_t);
			next_loc[0]=18;
			links[0].x=364;
			links[0].y=1088;
			links[0].w=636-364;
			links[0].h=1120-1088;
		break;
		case 17:
			load_location(l17_t);
			next_loc[0]=1;
			links[0].x=828;
			links[0].y=972;
			links[0].w=1134-828;
			links[0].h=998-972;
		break;
		case 18:
			load_location(l18_t);
			next_loc[0]=19;
			next_loc[1]=20;
			links[0].x=1130;
			links[0].y=2000;
			links[0].w=1394-1130;
			links[0].h=2026-2000;
			links[1].x=44;
			links[1].y=2040;
			links[1].w=290-44;
			links[1].h=2070-2040;
		break;
		case 19:
			load_location(l19_t);
			next_loc[0]=21;
			next_loc[1]=22;
			links[0].x=1544;
			links[0].y=1475;
			links[0].w=1810-1544;
			links[0].h=1500-1475;
			links[1].x=450;
			links[1].y=1670;
			links[1].w=1260-450;
			links[1].h=1700-1670;
		break;
		case 20:
			load_location(l20_t);
			next_loc[0]=21;
			links[0].x=45;
			links[0].y=1590;
			links[0].w=400-45;
			links[0].h=1614-1590;
		break;
		case 21:
			load_location(l21_t);
			next_loc[0]=1;//the END
			links[0].x=45;
			links[0].y=2033;
			links[0].w=1875-45;
			links[0].h=3062-2033;
		break;
		case 22:
			load_location(l22_t);
			next_loc[0]=1;
			links[0].x=352;
			links[0].y=895;
			links[0].w=1070-352;
			links[0].h=927-895;
		break;
	}
	pnl = 0;
	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = 1920;
	srcrect.h = 1080;
	dstrect.x = 0;
	dstrect.y = 0;
	dstrect.w = 1920;
	dstrect.h = 1080;

	SDL_Surface *buff,*buffo;
	buff = SDL_CreateRGBSurface(SDL_SWSURFACE, 1920, 1080, 32, rmask, gmask, bmask, amask);
	SDL_FillRect(buff, NULL, SDL_MapRGB(buff->format, 0, 0, 0));
	buffo = SDL_ConvertSurface(buff, screen->format,0);
	SDL_BlitSurface(buffo,&srcrect,screen,&dstrect);	
	SDL_BlitSurface(optimg,&srcrect,screen,&dstrect);
    SDL_Flip(screen);
	SDL_FreeSurface(buffo);
	SDL_FreeSurface(buff);
	linkon = 1;
}

void outro(void)
{
//FIXME
}

void Blit(void)
{
	linkon = 0;
	while(linkdraw) ;
	SDL_BlitSurface(optimg,&srcrect,screen,&dstrect);
    SDL_Flip(screen);
	linkon = 1;
}

int main( int argc, char* args[] )
{
	Mix_Music *music = NULL;
	int nosound=0;
	int quit=0;
	// clipping rectangles for blit
	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = 1920;
	srcrect.h = 1080;
	dstrect.x = 0;
	dstrect.y = 0;
	dstrect.w = 1920;
	dstrect.h = 1080;
 
    //Start SDL
   if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		return 0;
    }
	SDL_ShowCursor(SDL_DISABLE);
	const SDL_VideoInfo* videoInfo = SDL_GetVideoInfo ();
	
	int systemX = videoInfo->current_w ;
	int systemY = videoInfo->current_h ;
	Uint8 bpp = videoInfo->vfmt->BitsPerPixel ;
	
	if (systemX != 1920 || systemY != 1080)
	{
		printf("Framebuffer resolution of 1920x1080 is needed to run this game...Yours is %d x %d\n",systemX,systemY);
		printf("Change parameters hdmi_group & hdmi_mode in /boot/config.txt to have 1920x1080 resolution\n");
		printf("e.g. hdmi_group=1 hdmi_mode=5 for 60Hz TV sets\n");
		printf(" hdmi_group=2 hdmi_mode=82 for monitors\n");
		printf(" disable overscan!\n");
		printf("For further info see https://www.raspberrypi.org/documentation/configuration/config-txt/video.md\n");
//		return 0;
		systemX = 1920; systemY=1080;
	}

	Uint32 vflags = SDL_SWSURFACE;//|SDL_FULLSCREEN|SDL_NOFRAME;
//	Uint32 vflags = SDL_HWSURFACE;
    //Set up screen
    screen = SDL_SetVideoMode(systemX, systemY, bpp, vflags);
    if (!screen)
    {
        printf("SDL_SetVideoMode failed\n");
        return 0;
    }
	// load support for the PNG image format
	if((IMG_Init(IMG_INIT_PNG)&IMG_INIT_PNG) != IMG_INIT_PNG)
	{
    	printf("IMG_Init: Failed to init required png support!\n");
	    printf("IMG_Init: %s\n", IMG_GetError());
    	return 0;
	}

	//Set up sound
	if(Mix_Init(MIX_INIT_MP3)&MIX_INIT_MP3 != MIX_INIT_MP3) {
    printf("Mix_Init: Failed to init required mp3 support!\n");
    printf("Mix_Init: %s\n", Mix_GetError());
	printf("No sound will be played\n");
	nosound = 1;
    // handle error
	}

/*
    //Set up screen
    screen = SDL_SetVideoMode( 1920, 1080, 16, SDL_ANYFORMAT );
    if (!screen)
    {
     	printf("SDL_SetVideoMode failed\n");
     	return 0;
    }
  */ 

	//Initialize SDL_mixer
	if (!nosound) {
	    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1*1024) == -1)
    	{
        	printf("Mixer failed to open audio.\n");
			nosound = 1;
    	}
 	}

	if (!nosound) {
		music = Mix_LoadMUS("nihilum.mp3");
    
    	//If there was a problem loading the music
	    if(music == NULL)
    	{
        	printf("Mixer failed to load mp3.\n");
			nosound = 1;
    	}
 	}

	if (!nosound) {

		if(Mix_PlayMusic(music, -1)==-1) {
    		printf("Mix_PlayMusic: %s\n", Mix_GetError());
		}
	}

	if( TTF_Init() == -1 )
    {
		printf("Cannot initialize TTF support!\n");
        return 0;
    }

	font = TTF_OpenFont("font.ttf", 100);
	if (font == NULL) 
	{
		printf("Cannot open font.ttf!\n");
		return 0;
	}
	sfont = TTF_OpenFont("unispbi.ttf", 100);
	if (font == NULL) 
	{
		printf("Cannot open unispbi.ttf!\n");
		return 0;
	}

	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,SDL_DEFAULT_REPEAT_INTERVAL);

	intro();		

	set_loc(loc);
	//timer for link effect
	SDL_TimerID timerID = SDL_AddTimer(25, render_link, screen);

 while(!quit)
 {
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				quit = 1;
			break;
//-----------------KEY DOWN----------------------------
			case SDL_KEYDOWN:
				SDL_RemoveTimer(timerID);
				linkon = 0;
				while(linkdraw) ;
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					render_text(screen,font,quit_t,460,500);
					int dec=0;
					while(!dec)
					{
						while (SDL_PollEvent(&event))
						{
							if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_y) dec = 1;
							if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_n) dec = 2;
						}
					}
					if (dec == 1)  quit = 1;
					else 
					{
						Blit();
					}
				}
				if (event.key.keysym.sym == SDLK_RETURN)
				{
					if (link_visible)
					{
						loc = next_loc[pnl];
						//loc++;
						SDL_FreeSurface(optimg);
						if (loc>0) set_loc(loc);
						else 
						{
							outro();
							quit = 1; // ?
						}
					}
				}
				if (event.key.keysym.sym == SDLK_TAB)
				{
					pnl++;
					if (next_loc[pnl] == 0) pnl = 0;
					Blit();
				}
				if (event.key.keysym.sym == SDLK_DOWN)
				{
					if (optimg->h > 1080 ) {
						srcrect.y += KB_SCROLL_STEP;
						if (srcrect.y > optimg->h - 1080) srcrect.y = optimg->h - 1080;
						else Blit();
					}
				}
				if (event.key.keysym.sym == SDLK_UP)
				{
				srcrect.y -= KB_SCROLL_STEP;
				if (srcrect.y <0) srcrect.y = 0;
				else Blit();
				}
				linkon = 1;
				SDL_TimerID timerID = SDL_AddTimer(25, render_link, screen);
			break;
		}
	}
 }

	printf("(c)Yerzmyey & Hellboj 2018\nThis publication has been financed within a program of the Polish Minister of Science and Higher Education under the name ""National Program for the Development of Humanities"" in 2016-2019 (number 0020/NPRH4/H2b/83/2016).\n");
	while(linkdraw) ;
	SDL_RemoveTimer(timerID);
	IMG_Quit();	
	Mix_CloseAudio();
	while(Mix_Init(0)) Mix_Quit();
	Mix_FreeMusic(music);
	SDL_FreeSurface(optimg);
	SDL_FreeSurface(screen);
    SDL_Quit();
    return 0;
}
