  txt1.tap  �? � ���?�	--- Zupe�nie ci� nie rozumiem --- m�wi�a Judyta z
rozdra�nieniem. --- Raz twierdzisz, �e jeste� w tym dobry, innym
razem znowu narzekasz, �e twoje pisanie jest do niczego. To bez
sensu.
	Karl leniwie przygl�da� si� przez okno oty�ej s�siadce z
naprzeciwka, kt�ra z podziwu godn� kondycj� usi�owa�a z�apa�
swego yorkshire terriera. Miniaturowe zwierz�tko mia�o jednak
ochot� nacieszy� si� odrobin� wolno�ci, a by�o przy tym
niezwykle zwinne.
	--- Nie ma w tym sprzeczno�ci --- odpar� z przek�sem. ---
Pisz� kiepsko, ale wszyscy inni s� ode mnie znacznie gorsi.
W�a�nie to najbardziej mnie mierzi: ksi��ki si� sprzedaj�, wi�c
pozornie jestem dobry, cho� tak naprawd� nie ma w tym �adnej
mojej zas�ugi.
	--- Arogancki dupek! --- Z�o�� Judyty by�a niek�amana. ---
Nie rozpozna�by� dobrej literatury, nawet gdyby ci usiad�a bez
majtek na twarzy!
	Uzna�, �e to �wietny moment na zmian� tematu rozmowy.
	--- S�uchaj, ca�y dzie� chcia�em ci� o to zapyta� -- co si�
sta�o z twoim ulubionym bohomazem, kt�ry wisia� tam na �cianie?
	Kobieta spojrza�a na niego zdziwiona.
	--- Jakim bohomazem? Przecie� na tej �cianie nic nigdy nie
wisia�o.
	By� tak zaskoczony, �e zupe�nie nie wiedzia�, co
odpowiedzie�.
	Rozleg� si� przeci�g�y d�wi�k samochodowego klaksonu, a
potem pisk opon -- i jeszcze jeden pisk -- b�lu i przera�enia --
prawie zag�uszony tym pierwszym. York wpad� pod przeje�d�aj�ce
auto.
	Karl wsta� od sto�u.
	--- Id� na g�r� --- rzuci�. --- Mo�e uda mi si� troch�
popracowa�. [Zawsze wola� happy endy.] 
	W nast�pnych dniach Karl nie mia� zbyt wiele czasu, by
rozwa�a� kwesti� znikaj�cego arcydzie�a, musia� bowiem
ca�kowicie odda� si� pracy: terminy wydawnicze nagli�y, a jego
ksi��ka nadal daleka by�a od uko�czenia. Ostatecznie zdecydowa�
si� przedstawi� wydawcy maszynopis w wersji wst�pnej, aby w
og�le m�c udowodni�, �e podejmuje jakiekolwiek dzia�ania w celu
wywi�zania si� z umowy. Zazwyczaj stara� si� post�powa�
uczciwie, nie tyle nawet z potrzeby sumienia, ju� bardziej
dlatego, �e dawa�o mu to komfortowe poczucie wy�szo�ci wobec
innych. Ale i tak zawsze powtarza�: "Ka�dy, kto jest uczciwy,
�yje kr�tko, ubogo i bez szacunku bli�nich". "Kr�tko -- je�li ma
szcz�cie" -- dodawa� po zwyczajowej efektownej pauzie.
	Zdecydowa� si� przespacerowa� przez �r�dmie�cie, czemu
sprzyja�a s�oneczna pogoda. W takich razach mia� zwyczaj
pods�uchiwania rozm�w przechodni�w. Te wyrwane z kontekstu
sentencje zawsze wydawa�y mu si� fascynuj�ce.
	--- Wymy�lili te smartfony --- m�wi� gor�czkowo jaki� facet
do swego korpulentnego kolegi --- i ludzie nie kasuj� ju�
SMS-�w; przechowuj� cz�sto ca�� t� korespondencj�... Sprawdzi�em
ostatnio i przez ca�y okres zwi�zku wymienili�my z ni� dok�adnie
siedemset wiadomo�ci. Pieprzony, kurwa, romans siedmiuset
SMS-�w!
	--- ...nie ple� bzdur, demokracja przedstawicielska jest jak
od�ywianie przedstawicielskie lub przedstawicielskie uprawianie
seksu --- perorowa�a do s�uchawki telefonu przystojna blondynka
w �rednim wieku. --- Nie. Absolutnie nie! R�wnie dobrze mo�esz
powiedzie�, �e aby zlikwidowa� globalne ocieplenie, nale�y
w��cza� wi�cej lod�wek, bo wtedy zrobi si� ch�odniej. Idiota!
--- za�mia�a si�, najwyra�niej rozbawiona reakcj� rozm�wcy.
	Karl szed� niespiesznie wzd�u� szpaleru budynk�w, delektuj�c
si� rze�kim porankiem oraz pods�uchanymi wytworami losowo
napotkanych ludzkich umys��w. W kt�rym� momencie zauwa�y�
jednak, �e co� by�o nie w porz�dku. Szpaler nieoczekiwanie si�
sko�czy�: w miejscu, w kt�rym jeszcze wczoraj tkwi� budynek
jednego z najwi�kszych centr�w handlowych w mie�cie, znajdowa�
si� teraz pusty plac.
	Karl pocz�� biega� od �ciany do �ciany w poszukiwaniu
jakichkolwiek znak�w wczorajszej obecno�ci budynku. Nic, ani
�ladu. W pierwszym impulsie chcia� wypyta� o zjawisko pierwszego
napotkanego przychodnia, ale z jakiego� niezrozumia�ego powodu
mia� wyra�ne przeczucie, co te� us�yszy w odpowiedzi. W ko�cu
si� przem�g�, przecie� nie m�g� tak sta� bez ko�ca na g�adkim
trawniku.
	--- Przepraszam pana... --- Sprzedawca hot dog�w codziennie
sta� w tym miejscu, po przeciwnej stronie ulicy, on musia�
wiedzie�, co si� sta�o. --- Gdzie podzia�o si� centrum handlowe?
Przeprowadzali jakie� kontrolowane wyburzanie? Ale w takim
tempie...? Gdzie gruzowisko...?
	Handlarz przygl�da� mu si� z narastaj�cym zdziwieniem i
niech�ci� -- najwyra�niej mia� go za szale�ca.
	--- Panie, przecie� tu nigdy nic takiego nie sta�o,
czterna�cie lat ju� tu sprzedaj�. Id� pan st�d, straszysz pan
klientel�!
	Gdy stonowaniu uleg� pierwszy napad paniki, Karl zrozumia�,
�e nadesz�a pora, by podj�� decyzj� -- co dalej. Jak potraktowa�
to, co si� dzieje? Jakie przyj�� wobec tych wydarze� stanowisko?
Widzia� dwie alternatywy: dopu�ci, �e w rzeczy samej jest
wariatem i [uda si� do psychiatry] lub uzna, i� jednak realnie
dzieje si� co� z�ego i [postara si� zasi�gn�� porady naukowca]. 
	W oczekiwaniu na swoj� kolej wej�cia do gabinetu psychiatry,
Karl poczu�, �e musi zapali�. Mia� mieszane uczucia w zwi�zku z
na�ogiem, bowiem jego ojciec umar� na raka. "Co ciekawe, ojciec
nigdy nie pali�, a i tak wyniszczy� go nowotw�r -- wi�c jaki
w�a�ciwie sens mia�oby unikanie tytoniu" -- rozwa�a�, si�gaj�c
po ukryt� na takie okazje paczk� gauloises'�w.
	Ojciec przez ca�e lata walczy� z rakiem, ciesz�c si�
stosunkowo dobrym komfortem �ycia, a dopiero w ostatnich
miesi�cach uleg� nag�emu wyniszczeniu organizmu; [ide facto]
choroba w ci�gu tygodnia przyku�a go do ��ka. Karl do ko�ca si�
nim opiekowa�, ale w�a�nie w tym okresie obydwaj mieli trudno�ci
z nawi�zaniem jakiej� d�u�szej rozmowy, a nawet podczas kr�tkiej
wymiany zda� starali si� nie patrze� na siebie, tylko gdzie� na
boki, tak jakby to powolne umieranie wprawia�o ich obu w stan
zak�opotania.
	Tymczasem zosta� poproszony do gabinetu. Doktor Nadia
Morozova sta�a ty�em; nie zauwa�y�a, �e ju� go wpuszczono. Karl
przez pewien czas przygl�da� si� w milczeniu, jak poprawia szwy
na po�czochach. Znali si� od wielu lat i dawniej nawet ��czy�o
ich co� g��bszego, lecz teraz by�a to ju� jedynie historia. W
d�ugotrwa�ym zwi�zku ludzie mog� zacz�� odczuwa� niech�� do
swych partner�w, cz�sto obwiniaj�c ich o mas� nieistotnych
rzeczy, nierzadko zupe�nie nies�usznie. Dzieje si� tak, bo
kochankowie usi�uj� w ten spos�b ukry� fakt, �e w istocie s� �li
na samych siebie o to, �e nie potrafi� ju� dostrzec w partnerze
tego [iczego�,] co wci�� umiej� zobaczy� w nim ludzie obcy. Czyli
m�wi�c po ludzku -- bywa to nierzadko syndrom psa ogrodnika. By�
mo�e oboje zdawali sobie z tego spraw�, bowiem pozosta�o�ci� po
ich zwi�zku by� jaki� rodzaj zaufania, je�li nie przyja��.
	Zazwyczaj to on m�wi�, a ona dawa�a mu si� wygada�, i do tej
pory zdawa�o to egzamin.
	Wys�ucha�a, co mia� do powiedzenia i zastanowi�a si� przez
chwil�.
	--- I zdecydowa�e� si� jednak przyj�� z tym do mnie...
	--- Wiesz, jak to jest. Z m�drymi si� rozmawia, z g�upimi
si� przemilcza. --- U�miechn�� si�.
	--- Uwa�asz wi�c, �e masz zaburzenia psychiczne? Musz�
wiedzie�, jakie jest twoje wewn�trzne nastawienie do tego, co
ci� spotyka. Czy zdajesz sobie spraw�, �e jest to ca�kowicie
nierealne?
	--- Pytasz, czy jestem wariatem? Nie wiem, my�la�em, �e
wydanie takiej diagnozy jest raczej twoj� rol�. --- Karl za�mia�
si� nerwowo. --- Zastanawia�em si� kiedy� nad tym; je�li cech�
umys�u ludzkiego jest ci�g�e interpretowanie oraz
fabularyzowanie, to sny mog� by� jedynie pr�bami
zinterpretowania i sfabularyzowania procesu nie maj�cego [inic]
wsp�lnego z samym do�wiadczeniem rzeczywisto�ci: procesu
przetwarzania danych zebranych za dnia. Zak�adam, �e umys� u�ywa
w�wczas mo�liwie najdalszych skojarze� -- aby upodobni� jako� do
swojskiej rzeczywisto�ci procesy skrajnie odleg�e od "dziennej"
pracy m�zgu; procesy, kt�re przypominaj� raczej prac� komputera
przetwarzaj�cego dane ni� prac� �wiadomego umys�u. Rozumiesz, o
co chodzi -- przyk�ad skojarzenia bliskiego: dzwoni dzwonek u
drzwi, a �pi�cemu �ni si� w tym momencie, �e kto� dzwoni do
drzwi, wchodzi i tak dalej; podobne zjawiska zna chyba ka�dy z
nas. �ni�cy umys� reaguje na impuls ze �wiata zewn�trznego. Mam
tu na my�li, �e gdyby wspomniany dzwonek przekszta�ci� si� we
�nie w d�wi�k dzwonu ko�cielnego lub upadaj�cej szklanki --
r�wnie� uzna�bym to za skojarzenia bliskie.
	--- Jaki to ma zwi�zek z twoim stanem? --- zapyta�a Nadia.
	--- Mam wra�enie, �e podobne zjawisko mo�e mie� miejsce
podczas choroby psychicznej: uszkodzony umys� wykonuje
najdziwniejsze i najbardziej karko�omne operacje, prawdopodobnie
nawet zupe�nie bezsensowne, a wci�� jaka� jego cz�� usi�uje je
interpretowa� i fabularyzowa�, u�ywaj�c mechanizmu podobnego do
tego, kt�ry dzia�a podczas snu. Taka wzmo�ona praca odbiera
mo�liwo�ci i czas umys�u na kontakt z rzeczywisto�ci�, jest on
wi�c prawie �aden albo wr�cz zerowy. Najdalsze (b�d� rzadziej --
nieco bli�sze) skojarzenia staj� si� wtedy podstawow� zasad�
dzia�ania umys�u, swego rodzaju drog� chorego do �wiata
zewn�trznego -- ale jednocze�nie drog� (kluczem) dla terapeuty
do wn�trza �wiata chorego. My�l� jednak, �e u�ycie tej jedynej
drogi dotarcia do zwichrowanego umys�u b�dzie zadaniem niezwykle
trudnym lub wr�cz niewykonalnym, bo cz�owiek o umy�le pracuj�cym
w spos�b klasyczny nie jest w stanie poj�� tor�w skojarzeniowych
umys�owo�ci chorego; moim zdaniem zdrowy pos�uguje si� nimi
tylko podczas snu, a na jawie ka�dy z nas u�ywa jedynie
skojarze� bliskich.
	Kobieta zanotowa�a co� w le��cym na biurku zeszycie.
	--- Jak wobec tego wygl�daj� skojarzenia odleg�e? ---
spyta�a po chwili.
	--- R�nica jest tu tak wielka, �e nawet nie potrafi�bym
poda� przyk�adu skojarzenia dalekiego, gdy� tylko jeden jego
kraniec znajduje si� w dobrze nam znanej rzeczywisto�ci. Drugi
znajduje si� przecie� w niewyobra�alnym �wiecie szale�stwa.
Czyli bliskie skojarzenia to takie, kt�rych oba kra�ce tkwi� w
naszej rzeczywisto�ci, w kt�rej obowi�zuje zasada relewancji --
do skojarze� u�ywamy kilku najbardziej charakterystycznych cech
obiektu. My�l�, �e te dalekie, czy odleg�e skojarzenia nie
bazuj� na tej zasadzie -- umys� �pi�cego b�d� chorego, b�d�c
zmuszonym do fabularyzowania element�w biegunowo odleg�ych od
rzeczywisto�ci, �apie si� (niemal�e jak ton�cy brzytwy) cech
wybranych losowo, wcale nie tych najbardziej
charakterystycznych. Bo te� rzeczy, kt�re jest zmuszony
kojarzy�, nie maj� ze sob� nic (b�d� prawie nic) wsp�lnego.
	--- A ja my�l�, �e powiniene� z tym i�� do psychiatry, z
kt�rym si� jeszcze nie pieprzy�e� --- rzuci�a pani doktor
profesjonalnym tonem, przepisa�a pacjentowi pigu�ki, zaleci�a
miesi�czny odpoczynek od literatury [iscience fiction;] kaza�a si�
wynosi� i wr�ci�, gdyby problemy si� powt�rzy�y.
	Karl by� ju� przy drzwiach, ale jeszcze odwr�ci� si� i
podj��:
	--- Nadia, ja...
	Zamar�. W gabinecie by� ju� sam. Znikn�a Nadia, znikn�y
meble i wyposa�enie. Pok�j by� ca�kowicie pusty. Wypad� do
poczekalni -- to samo. Go�e �ciany, ani �ywej duszy.
	Karl z przera�eniem [wybieg� z budynku na ulic�]. 
	Po drodze do domu uwa�nie obserwowa� okolic�; mia� przemo�ne
wra�enie, �e na ulicach jest mniej ludzi ni� zwykle. W duchu
liczy� na to, �e tylko mu si� tak wydaje. Powa�nie niepokoi� si�
r�wnie� o los Judyty. Czy zastanie j�, gdy wr�ci? Co z tego, �e
czasami go irytowa�a. By� mo�e to ostatnie wypadki otworzy�y mu
oczy, ale wiedzia� ju�, �e drugiej takiej na swej drodze zapewne
nie spotka. Zreszt� uzna�, �e odrobina intelektualnego
mordobicia dobrze robi na kr��enie.
	"Niech ona b�dzie ca�a i zdrowa, Bo�e..." --- my�la� z
przera�eniem, powoli naciskaj�c klamk�. --- "Bo�e, pom�... a
je�li nie istniejesz, to by�oby to z twojej strony wielkie
kurewstwo".
	--- Aaa, jeste�! --- Judyta otworzy�a mu drzwi, s�ysz�c, jak
nieudolnie gmera przy zamku. --- Zastanawia�am si� ju�, gdzie
ci� wessa�o.
	Karl nie umia�by opisa� ulgi, jaka go w tej chwili ogarn�a,
nawet gdyby pr�bowa�. Ale nie pr�bowa�. Obj�� tylko Judyt� i
przycisn�� do siebie najmocniej, jak potrafi�.
	--- Udusisz mnie, wariacie! --- Za�mia�a si�. --- Te� si�
ciesz�, �e ci� widz�. No wchod� ju�.
 
	Przez kilka nast�pnych dni Karl wraca� do siebie po
ostatnich przej�ciach; usi�owa� zapomnie� o dziwnych
wydarzeniach i �y� normalnie. Cieszy� si� tym, co ma. Wcze�niej
chyba nie do ko�ca zdawa� sobie spraw�, jak bardzo jest to dla
niego wa�ne -- dom, rozmowy z Judyt�, czasem nawet k��tnie, jego
praca nad kolejnymi ksi��kami...
	Czasami Judyta wypytywa�a go o t� pogo� za fikcj�.
	--- Po co ci w�a�ciwie to tworzenie r�nych historii? Gdy
nic nie piszesz, to znowu czytasz, ogl�dasz mas� film�w,
seriale...? To jaki� rodzaj eskapizmu?
	Karl za� niezmiennie odpowiada�:
	--- Po prostu uwa�am, �e �ycie jest za kr�tkie. �wiadomo��
nieuchronnej �mierci generuje we mnie konieczno�� przed�u�enia
sobie �ycia na wszelkie mo�liwe sposoby. Nawet je�li to
przed�u�enie pozorne. Wch�aniam w ten spos�b �ycie innych; tak
wiele �y�, r�wnie� tych fikcyjnych, jak to tylko mo�liwe. Ka�da
opowie�� to czyje� �ycie, kt�re mog� obserwowa�; rozumiesz --
prze�y� wraz z bohaterem. Nie wiem, co mnie czeka po �mierci --
by� mo�e wieczna nico��. W por�wnaniu do niej -- jedno moje
kr�tkie �ycie to stanowczo zbyt ma�o. To skandalicznie
niewystarczaj�co. Staram si� w ten spos�b kompensowa� swoje
nadchodz�ce nieistnienie. Nigdy nie wiesz, kiedy sko�czy si�
�wiat.
	Niekiedy jej to wystarcza�o i odchodzi�a nieco zanudzona
jego, jak to z u�miechem okre�la�a, "smutnym pieprzeniem
niespe�nionego intelektualisty". Ale bywa�o, �e podejmowa�a
temat:
	--- My�l�, �e to samo dotyczy historii, wiesz, jako wiedzy o
tocz�cych si� dziejach. Zobacz, ile mamy informacji, kt�re by�
mo�e traktujemy pod�wiadomie jako pewien rodzaj wspomnie�.
Wiemy, co dzia�o si� w �redniowieczu, w antyku... Dzi�ki
rekonstrukcjom, filmom dokumentalnym, szlag, nawet dzi�ki
bzdurom spod znaku p�aszcza i szpady mamy jakie� wyobra�enie o
wygl�dzie tych ludzi, zwyczajach, sposobie m�wienia. Wiedza o
�wczesnym poziomie medycyny, o higienie -- i kuchni -- pozwala
nam nawet wyobrazi� sobie zapach danej epoki. Nie �yli�my wtedy,
ale mamy wra�enie, �e jest inaczej. Bo przecie� to wszystko
takim sposobem "pami�tamy". Gdy si� nad tym zastanowi� -- jak�e
rozleg�e staje si� dzi�ki temu nasze �ycie, jak bardzo rozci�ga
si� w czasie!
	--- Zgadza si�. Jest to pierwsza po�owa problemu. Bo
rozci�gamy, jak to okre�lasz, nasze �ycia w obie strony. Owszem,
chcieliby�my �y� od zawsze, a pr�cz tego �yczyliby�my sobie �y�
wiecznie, w sensie od dzi� po wsze czasy: wbijamy si� wi�c,
skoro nie cia�em, to przynajmniej umys�em, jak najdalej w
przysz�o��. St�d futurologia, st�d SF. Daj� nam poczucie, �e
wiemy, jak b�dzie wygl�da� przysz�y �wiat -- "rozci�gaj� nasze
�ycie do przodu". Dlatego pisz�. Chocia� ma�o kto w og�le to
czyta.
	--- Bez przesady. Przecie� widzia�am -- pisali o tym kawa�ku
sprzed paru lat, �e to "powie�� kultowa".
	--- Wygl�da na to, �e je�eli wystarczaj�co d�ugo poczekasz,
to nawet g�wno pokryje si� szlachetn� patyn�. Zreszt� tak zwana
kultowo�� jest nie tyle wynikiem pracy autora ani jako�ci
dzie�a, ile raczej rezultatem dzia�ania umys�u odbiorcy.
 
	Kt�rego� dnia Karl pr�bowa� odnale�� w starych papierach akt
zgonu ojca. Dokumentu nigdzie nie by�o. Zadzwoni� do urz�du
stanu cywilnego, kt�ry wystawia� akt.
	--- Przykro mi, niestety nie mam �adnych informacji na ten
temat. --- W g�osie urz�dniczki pobrzmiewa�a podejrzliwo��,
kt�r� Karl ju� niestety zna�. --- Musia� pan pomyli� oddzia�.
Gdyby cokolwiek by�o u nas za�atwiane, na pewno pozosta�by tego
�lad.
	Mia� w�t�� nadziej�, �e to pomy�ka, zatelefonowa� wi�c
jeszcze do szpitala, kt�ry wystawia� w�wczas kart� zgonu.
	--- Nie. Niestety nie. --- S�uchaj�c g�osu lekarza dy�urnego
w s�uchawce, Karl bezwiednie wyobrazi� sobie m�czyzn� niskiego
i �ysego. Zacz�� zastanawia� si�, czy cz�owiek mo�e brzmie�
�yso. --- �adnej adnotacji, brak historii choroby. Osoba o tym
nazwisku w og�le nie by�a u nas leczona. Do widzenia.
	Po wizycie w kilku innych urz�dach okaza�o si�, �e ani
ojciec, ani matka w og�le nie istnieli. Nie by�o o nich
jakichkolwiek zapis�w ani dokument�w. Poczu�, �e d�u�ej ju� tego
nie wytrzyma, �e mo�e zrobi� sobie co� z�ego. Najwyra�niej
nadesz�a pora, by podj�� decyzj�, czy w spraw� wtajemniczy�
kogo� jeszcze. Karl wiedzia�, �e sam ju� tego nie ud�wignie.
Rozwa�a�, czy [	opowiedzie� o wszystkim bratu] i poprosi� go o
pomoc. A mo�e jednak [
nie ryzykowa�]...? A nu� Anton nie uwierzy
mu i uzna histori� za objaw powa�nej choroby psychicznej...? 
�