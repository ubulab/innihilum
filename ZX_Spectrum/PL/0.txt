	Karl zadecydowa�, �e nie podzieli si� ci�arem z nikim
innym. Mia� nadziej�, �e jako� to zniesie, �e sam dojdzie
przyczyn tej powolnej atrofii rzeczywisto�ci -- mo�e nawet jej
zapobiegnie. Odwiedza� kolejnych naukowc�w, bada� teorie
spiskowe...
	Tymczasem ulice wyludnia�y si�, infrastruktura miejska
zanika�a. W niepami�� odchodzi�y nawet kolejne cechy
topograficzne terenu. Wzg�rza, jeziora, pobliski las. Ci ludzie,
kt�rzy jeszcze pozostali, nie mieli poj�cia, �e to, co ich
otacza, to jedynie marny kikut realno�ci. W�a�nie to wzbudza�o w
Karlu najwi�ksze przera�enie. By� jak chory na Alzheimera, kt�ry
popada w coraz g��bsz� depresj�, lecz tu r�nica by�a taka, �e
to nie kolejne wspomnienia znika�y -- tu odchodzi�y kolejne
elementy �wiata.
	Coraz cz�ciej my�la� o rodzicach, zw�aszcza o ojcu. Nie
zapami�tujemy kochanej zmar�ej osoby tak�, jak� by�a w swych
najlepszych latach, za m�odu, w pe�ni �ycia i zdrowia.
Chcieliby�my, ale tak nie jest. Je�eli byli�my przy kim� takim
do ko�ca, to ju� na zawsze zapami�tamy go jako cz�owieka
s�abego, skurczonego chorob� lub wiekiem (albo jednym i drugim),
nieb�d�cego w stanie wykona� samodzielnie najprostszej nawet
czynno�ci.
	To wtedy, gdy mamy szcz�cie. Poniewa� czasami nie
zapami�tujemy zmar�ych najbli�szych nawet takimi, jakimi byli za
�ycia. Je�li bowiem zdarzy�o nam si� by� przy ich �mierci, ten
przyt�aczaj�cy widok ju� na zawsze z nami pozostanie i zwyci�y,
pokona, st�amsi wcze�niejsze, dobre wspomnienia.
	M�wi si�, �e dawni wojownicy woleli raczej zgin�� na polu
walki ni� umrze� w �o�u ze staro�ci. Ujmowane jest to w
kategoriach bohaterstwa i odwagi. Karl uwa�a�, �e nie ma to nic
wsp�lnego ani z jednym, ani z drugim; przeciwnie, oni po prostu
dobrze wiedzieli, kt�ra �mier� jest straszniejsza --
zniedo��nia�a ze staro�ci, czy nag�a w walce. Nie by�o to wi�c
�adne bohaterstwo, a [ide facto] ucieczka.
	Zawsze zastanawia� si�, czy gdyby gin�� nagle, na przyk�ad w
wypadku -- to umrze z okrzykiem "O Bo�e!", czy raczej: "Kurwa
ma�!". Gdy jednak zdecydowa� si� w ko�cu odej�� na w�asnych
warunkach i skorzysta� z zabytkowego Lugera P08 kaliber 9 mm,
pozostaj�cego w ich rodzinie od pokole� -- okaza�o si�, �e
[uczyni� to bez s�owa]. 
