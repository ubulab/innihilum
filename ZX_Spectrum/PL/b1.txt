	Sekta zazwyczaj rozdawa�a w mie�cie mas� ulotek, st�d
wiadomo by�o, w kt�rym rejonie znajduje si� jej siedziba. Id�c,
Karl czu� przygn�bienie i strach, bo widzia�, jak wiele obra�e�
odnios�a rzeczywisto��. Chocia� wyda�o mu si�, �e na ulicach
jest s�oneczniej ni� zwykle, to szybko zrozumia�, �e po prostu w
mie�cie brakuje powa�nej cz�ci zabudowa�. Na chodnikach pomimo
godzin szczytu by�y niemal�e pustki. Ruch uliczny prawie zamar�.
	Anton zupe�nie nie zdawa� sobie z tego sprawy i szed� ra�no,
opowiadaj�c o swoich erotycznych podbojach.
	--- ...no i tak to wygl�da�o w zesz�ym tygodniu, a znowu
przedwczoraj pozna�em jedn� blondyn�. T� to d�ugo musia�em
namawia� na seks, ale w ko�cu zwyci�y�em.
	--- Opiera�a si�?
	--- Tak. O drzwi.
	Po drodze Karl rzuci� kilka monet �ebrakowi stoj�cemu pod
sklepem. "Lepiej niech je szybko wyda" --- pomy�la� z
niepokojem. --- "Mo�e nie mie� ju� na to wiele czasu".
	--- Po co dajesz pieni�dze kloszardom? --- zdziwi� si�
Anton. --- I tak to przepij�. Nie wiem, to tak, jakby� poca�owa�
prostytutk� w r�k� lub co� w tym gu�cie.
	--- Oby� si� kiedy� na w�asnej sk�rze nie nauczy�, �e �ebrak
to taki sam cz�owiek jak ty -- tylko jemu gorzej si� powiod�o
--- burkn�� Karl poirytowany. --- Sam wiesz, jak to jest --
dobrobyt jest wtedy, gdy najlepiej �yje si� przest�pcom, a
kryzys jest w�wczas, kiedy jedynie przest�pcom �yje si� dobrze.
A tak w og�le, je�li ju� musisz wiedzie�, to zdarzy�o mi si�
poca�owa� prostytutk� w r�k�.
	--- Jak to? --- Brat wpatrywa� si� w niego ze zdumieniem.
	--- Normalnie. Wychodzi�em i wypada�o si� po�egna�. Kobiecie
nale�y si� szacunek. Zreszt� odrobina dobrych manier jeszcze
nikogo nie zabi�a, prostaku.
	Dotarli na miejsce. Budynek szczelnie pokryty by� graffiti.
Ze �cian krzycza�y do nich napisy takie jak: "Jeste� czyim�
tamagotchi" albo "Wyj�cie z gry to grzech �miertelny" i podobne.
Karl, my�l�c o sekcie, oczekiwa� snuj�cych si� w ciszy postaci w
habitach lub w najlepszym razie w zwiewnych szatach. Tymczasem
zar�wno przed wej�ciem, jak i wewn�trz koczowa�a krzykliwa
pstrokata ha�astra, g��wnie w m�odym wieku. Ludzie o wygl�dzie
cybergot�w p�ci obojga najcz�ciej grali na handheldach, w
pomieszczeniach raczej na komputerach b�d� konsolach
stacjonarnych, ochoczo przyjmowali narkotyki w r�nej formie,
niekt�rzy publicznie uprawiali seks.
	Przyw�dca grupy powita� przyby�ych braci i kaza� nazywa� si�
opatem.
	--- ...chocia� ta kolorowa banda nazywa mnie dla jaj
komturem --- doda� z rozbawieniem.
	Wygl�da� zupe�nie normalnie, nie mia� makija�u, kolorowych
szkie� kontaktowych, maski ani wielobarwnego irokeza. Nie mia�
nawet piercingu czy tatua�u. Przynajmniej w widocznych
miejscach.
	--- Owszem --- m�wi� dalej --- ca�y wszech�wiat jest tylko
gr� komputerow�, w kt�rej uczestniczymy. W�a�ciwie s�owo "tylko"
jest tu nieadekwatne -- powinienem raczej u�y� s�owa "a�".
Przecie� w grze komputerowej mo�esz robi�, co chcesz: panuje u
nas ca�kowita wolno��, poniewa� wiemy, �e wszystko wok� nas i
tak nie jest realne.
	Anton zastanowi� si� chwil�.
	--- Ca�kowita wolno��? --- zapyta�. --- W��cznie z...
morderstwem?
	--- Oczywi�cie --- odpar� opat. --- Wierzymy, �e za fragi
mamy dodatkowe punkty. Zreszt�, je�eli z gry usuniesz innego
gracza -- on i tak ma przecie� wiele �y�, ostatecznie mo�e
r�wnie� rozpocz�� gr� od nowa. Poza tym jest du�e
prawdopodobie�stwo, �e i tak usun��e� NPC-a. Robimy, na co mamy
ochot� -- chodzi o to, by gra przynosi�a jak najwi�cej
przyjemno�ci. Po to si� gra, czy� nie?
	--- Chwileczk� --- Karl by� zdezorientowany. --- Czym jest
NPC?
	--- [iNon-player character] oczywi�cie. --- Opat spojrza� na
niego z dezaprobat�. --- Postaci wygenerowane przez komputer,
zaprogramowane do interakcji z graczami w celu uatrakcyjnienia
rozgrywki.
	--- Czyli... to jest jaki�... futurystyczny solipsyzm? ---
zapyta� Anton.
	--- Powiedzia�bym, �e wprost przeciwnie. Nie jeste�my
solipsystami, nie wyznajemy te� materialistycznego agnostycyzmu.
Solipsyzm g�osi, �e realnym jest jedynie podmiot poznaj�cy, za�
ca�a rzeczywisto�� jest tylko zestawem jego w�asnych
subiektywnych wra�e�, natomiast wszystkie napotkane osoby oraz
obiekty nieo�ywione s� niejako cz�ciami umys�u jednostki, kt�ra
ich do�wiadcza. My natomiast dopuszczamy mo�liwo�� zgo�a
przeciwn�: kt� zar�czy, �e to pan lub ja naprawd� istniejemy,
�e to akurat my jeste�my graczami? By� mo�e realnie wcale nas
nie ma -- mo�e tkwimy tu jedynie w formie NPC-�w, stanowi�cych
jedynie t�o dla jakiej� prawdziwej osoby -- gracza? Z tego, co
widz�, to panowie r�wnie� s� NPC-ami, tak kiepsko przygotowano
pan�w postaci.
	Opat stwierdzi� w ko�cu, �e problem Karla zupe�nie go nie
interesuje, i �e nie b�dzie po�wi�ca� wi�cej cennego czasu
nieistniej�cym osobom, musi bowiem zebra� wi�cej punkt�w w grze.
Jako �e mog�o to oznacza� zdobywanie kolejnych frag�w, bracia
po�piesznie opu�cili budynek.
	By�o ju� na tyle p�no, �e um�wili si� na kontynuacj� swych
poszukiwa� w dniu nast�pnym. [Karl zrezygnowany uda� si� do domu.] 
