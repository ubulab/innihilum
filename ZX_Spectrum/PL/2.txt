	Karl wola�by raczej unikn�� dekapitacji, lecz par� dni
p�niej pewna kwestia nie dawa�a mu spokoju. Ot� w pokoju
go�cinnym na g��wnej �cianie zawsze wisia� obraz. Kilka
czerwonych smug na ��tym tle, przez Judyt� szumnie nazywanych
"sztuk�". Wisia� -- do zesz�ej �rody. Od tamtego czasu �ciana
pozostawa�a pusta.
	Karl nie t�skni� za tym "dzie�em", ale trzeba by�o przyzna�,
�e bazgro�y posiada�y jednak pewn� warto�� rynkow�; poza tym
dziwi� si�, �e na �cianie nie pozosta� �aden �lad -- obraz
wisia� tu od lat, w jego miejscu powinna wi�c zia� du�a jasna
plama. By�o to zastanawiaj�ce.
	Rozwa�a�, czy [pyta� o to Judyt�], ryzykuj�c kolejne
spi�cie, czy te� lepiej odczeka� do jutra i [zagadn�� brata] --
mo�e Anton wiedzia�by co� na ten temat. 
