	Jeszcze kilka tygodni temu ko�ci� by� cz�ci� ca�ej grupy
zabudowa�. Dzisiaj sta� osamotniony, na pustkowiu, natomiast
kilkana�cie metr�w za ko�cio�em ko�czy� si� �wiat. Literalnie.
Karl przyj�� to odkrycie z zadziwiaj�cym opanowaniem. Ostro�nie
podszed� do ko�ca �wiata. O ile za Karlem panowa� dzie�; na
niebie �wieci� ten ogryzek, kt�ry pozosta� jeszcze ze S�o�ca, o
tyle przed jego oczami -- nagle i bez ostrze�enia --
rozpo�ciera�a si� czer� kosmosu z rozrzuconymi tu i �wdzie
bardzo nielicznymi ju� gwiazdami. Grunt urywa� si� wzd�u� tej
samej linii, ale Karl nie odwa�y� wychyli� si� przez kraw�d�,
poniewa� podejrzewa�, �e po drugiej stronie nie ma atmosfery,
nie wspominaj�c ju� o morderczej temperaturze -- nie mia�o dla�
zbyt wielkiego znaczenia, czy b�dzie to -180 stopni Celsjusza,
czy mo�e nawet zero absolutne; tak d�ugo, jak to tylko by�o
mo�liwe, nie mia� ochoty dozna� tego na w�asnej sk�rze.
Zw�aszcza, �e domniemanie jego uprawdopodobnia� kamie�, kt�ry
Karl rzuci� w stron� ko�ca �wiata w celach eksperymentalnych: po
przekroczeniu tej niesamowitej granicy nie lecia� on ju� po
�uku, tylko w linii prostej, nie zwalniaj�c -- oddala� si�
majestatycznie, by w ko�cu znikn�� w przestrzeni, kt�r� kiedy�
zwano mi�dzyplanetarn�.
	Kaplica by�a mroczna, pusta i cicha.
	--- Halo?! --- zawo�a� Karl, a w�asny g�os wyda� mu si�
upiorny w tej ciszy i ni�s� si� bardzo d�ugim echem, powoduj�cym
g�si� sk�rk�. --- Jest tu kto?!
	Rozleg� si� jaki� odleg�y ha�as, a potem z nawy bocznej, zza
szeregu kolumn wyszed� ksi�dz.
	--- Witaj, synu. Jestem plebanem tej parafii. W czym mog� ci
pom�c? --- przem�wi� matowym g�osem.
	Karl nie by� cz�owiekiem nazbyt pobo�nym. Przeczyta�
wystarczaj�co du�� cz�� Biblii, by wiedzie�, �e je�li przeczyta
ca��, to niew�tpliwie utraci resztki wiary. Przygl�dn�� si�
proboszczowi, kt�ry by� cz�owiekiem ju� niem�odym, lecz trzyma�
si� nie�le, a bujne siwe w�osy i liturgiczne szaty sprawia�y, �e
wygl�da� niemal�e dostojnie.
	--- Poszukujesz Boga, synu? Niebawem rozpoczniemy
nabo�e�stwo, czy zechcesz si� przy��czy�?
	--- Wie ksi�dz... My�l�, �e gdyby jakie� Wy�sze Byty
stworzy�y dla eksperymentu istoty inteligentne, lecz
zaprogramowane w ten spos�b, by dochodzi� do cel�w jak
najtrudniejszymi drogami, nie dostrzegaj�c rozwi�za�
oczywistych, to efektem finalnym byliby ludzie. My. Cz�owiek
zazwyczaj woli nie widzie� prawdy, za to natychmiast stworzy
sobie jej protez� i dopiero na jej podstawie dzia�a. Ze mn�
chyba jest tak, �e mo�e i wierz� w Boga... ale czy wierz�
Bogu...?
	--- O co wi�c chodzi? --- Ton proboszcza sta� si� ch�odny.
	--- Ja... Pisz� artyku� na temat... Ehm... Ko�cio�a
Katolickiego... Zawsze tu takie pustki? Mo�na wiedzie�, ilu
wiernych ma parafia?
	--- Od kiedy pe�ni� tu pos�ug�, a b�dzie ju� siedemna�cie
lat, nasza wsp�lnota sk�ada si� z pana Morgensterna. --- Ornat
plebana szele�ci� g�o�no przy ka�dym ruchu.
	--- Sk�ada si�... --- Karl usi�owa� zrozumie� t� dziwn�
sk�adni�. --- Chce mi ksi�dz powiedzie�, �e od siedemnastu lat
odprawia msz� dla jednej osoby? Nie wydaje si� to ksi�dzu
dziwne?
	--- Nie rozumiem, o co panu chodzi --- odrzek� sucho
proboszcz. --- To przecie� perfekcyjne i naturalne odwzorowanie
dualizmu B�g--ludzko��. Prawd� t� g�osi� r�wnie� nasz Pan Jezus
Chrystus oraz Jego Aposto�. Skoro pisze pan artyku�, powinien
pan lepiej zna� Testament.
	--- Ciekawe, kt�ry --- mrukn�� Karl. Pleban spojrza� na
niego zaskoczony.
	--- Jak to "kt�ry"? To jakie� blu�niercze �arty?! Jest tylko
[ijeden] Testament!
	Karl bez s�owa opu�ci� kaplic�.
	Sta� przez chwil� przed ko�cio�em i zastanawia� si�, co
jeszcze mo�e w�a�ciwie zrobi�. Profesor Benedikt, ostatni z
naukowych konsultant�w ksi��ek Karla, by� wprawdzie
ekscentrycznym starcem, ale -- o ile wiekowy naukowiec nadal
 
istnieje -- wygl�da�o na to, �e [inne opcje si� ju� wyczerpa�y.]
	Przed odej�ciem Karl spojrza� jeszcze raz w bezdenn� czer�,
odleg�� zaledwie o kilka metr�w. S�ysza�, �e czasami cz�owiek
stoj�cy nad przepa�ci� ma niezrozumia�� ochot�, by skoczy�. Mo�e
by tak p�j�� do dzwonnicy po lin� i jednak spr�bowa� sprawdzi�,
co [znajduje si� poza ko�cem �wiata]...? "Wyskoczy� z Ziemi w
kosmos -- to mo�e by� jedyna taka okazja" -- zastanawia� si�.
"Skoro powietrze nie ucieka z ziemskiej atmosfery przez t�
niezwyk�� granic�, to mo�e po drugiej stronie te� jest jaka�
atmosfera...?" Pomys� by� szale�czy, lecz czy� nie takie w�a�nie
by�y ostatnie tygodnie? 
