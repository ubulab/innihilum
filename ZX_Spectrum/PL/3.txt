	--- Zupe�nie ci� nie rozumiem --- m�wi�a Judyta z
rozdra�nieniem. --- Raz twierdzisz, �e jeste� w tym dobry, innym
razem znowu narzekasz, �e twoje pisanie jest do niczego. To bez
sensu.
	Karl leniwie przygl�da� si� przez okno oty�ej s�siadce z
naprzeciwka, kt�ra z podziwu godn� kondycj� usi�owa�a z�apa�
swego yorkshire terriera. Miniaturowe zwierz�tko mia�o jednak
ochot� nacieszy� si� odrobin� wolno�ci, a by�o przy tym
niezwykle zwinne.
	--- Nie ma w tym sprzeczno�ci --- odpar� z przek�sem. ---
Pisz� kiepsko, ale wszyscy inni s� ode mnie znacznie gorsi.
W�a�nie to najbardziej mnie mierzi: ksi��ki si� sprzedaj�, wi�c
pozornie jestem dobry, cho� tak naprawd� nie ma w tym �adnej
mojej zas�ugi.
	--- Arogancki dupek! --- Z�o�� Judyty by�a niek�amana. ---
Nie rozpozna�by� dobrej literatury, nawet gdyby ci usiad�a bez
majtek na twarzy!
	Uzna�, �e to �wietny moment na zmian� tematu rozmowy.
	--- S�uchaj, ca�y dzie� chcia�em ci� o to zapyta� -- co si�
sta�o z twoim ulubionym bohomazem, kt�ry wisia� tam na �cianie?
	Kobieta spojrza�a na niego zdziwiona.
	--- Jakim bohomazem? Przecie� na tej �cianie nic nigdy nie
wisia�o.
	By� tak zaskoczony, �e zupe�nie nie wiedzia�, co
odpowiedzie�.
	Rozleg� si� przeci�g�y d�wi�k samochodowego klaksonu, a
potem pisk opon -- i jeszcze jeden pisk -- b�lu i przera�enia --
prawie zag�uszony tym pierwszym. York wpad� pod przeje�d�aj�ce
auto.
	Karl wsta� od sto�u.
	--- Id� na g�r� --- rzuci�. --- Mo�e uda mi si� troch�
popracowa�. [Zawsze wola� happy endy.] 
